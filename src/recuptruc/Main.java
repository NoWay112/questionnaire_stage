package recuptruc;

import java.io.*;

public class Main { 
   public static void main(String[] args) {
      File dir = new File("\\\\storage2016.windows.dauphine.fr\\group\\Pedagogie\\CRIO\\Partage\\LOGUSER\\");
      FilenameFilter filter = new FilenameFilter() {
         public boolean accept (File dir, String name) { 
            return name.startsWith("test - A209BIS-5");
         } 
      }; 
      String[] children = dir.list(filter);
      if (children == null) {
         System.out.println("n'existe pas!"); 
      } else { 
         for (int i = 0; i< children.length; i++) {
            String filename = children[i];
            System.out.println(filename);
            System.exit(0);
         } 
      } 
   }
}