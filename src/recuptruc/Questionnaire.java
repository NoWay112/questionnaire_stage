package recuptruc;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.*;


public class Questionnaire {
	
	static PrintWriter sortie;
	static boolean sortie2;
	private static String nameFile;
	private static String nameFileS;
	
    //@SuppressWarnings("rawtypes")
	//static JComboBox<ArrayList> mycombo;
    	
    //fonction cr�ation du questionnaire
	public void buildGUI(){
			
		//declaration JFrame	
	    JFrame.setDefaultLookAndFeelDecorated(true);
	    JFrame f = new JFrame();
	    
	    //appel function qui rend non-quitable
	    f.setResizable(false);
	    removeMinMaxClose(f);
	    
	    //bouton OK
	    JPanel p = new JPanel(new GridBagLayout());
	    JButton btn = new JButton("OK");
	    
	    //declaration ds champs
		JTextField chNom = new JTextField(3);
	    JTextField chBur = new JTextField(3);
	    JTextField chSer = new JTextField(3);
	    JTextField chTel = new JTextField(3);
	    
	    //message d'introductions
	    //f.add(new JLabel ("<html><i><span face=\"verdana\" style='color:#0174DF ;font-size:15px;'>"+"Programme de recensement des postes � Dauphine veuillez remplir ce questionnaire. La Direction du Num�rique" +"</span></html>"));
	    //La direction num�rtique doit migrer votre poste sur un autre domaine de ce fait nous souhaiterions que vous remplisser ce questionnaire � fin de vous recenser
	    f.add(new JLabel ("<html><p><span face=\"verdana\" style='color:#2E64FE ;font-size:11px;'>"+"Prochainement nous allons migrer votre poste sur le domaine <u>windows.dauphine.fr</u>, vous �tes actuellement sur le domaine <u>workspace.windows.dauphine.fr</u>, pour cela nous vous demandons de remplir ce questionnaire pour nous permettre cette migration, merci pour votre aide." +"</span></center></html>"));

	    //f.add(new JLabel ("<html><span style='font-size:15px'>"+"La Direction du Num�rique." +"</span></html>"));

	    //ajout nom/prenom
	    f.add(new JLabel ("<html><u><span style='font-size:13px'>"+"Nom/Prenom:"+"</span></html>"));
	    f.add(chNom);
	    
	    //ajout bureau
	    f.add(new JLabel("<html><u><span style='font-size:13px'>"+"Num�ro de bureau:"+"</span></html>"));
	    f.add(chBur);

	    //ajout telephone
	    f.add(new JLabel("<html><u><span style='font-size:13px'>"+"Num�ro de telephone:"+"</span></html>"));
	    f.add(chTel);
	    
	    //ajout Sercices
	    f.add(new JLabel("<html><u><span style='font-size:13px'>"+"Nom du Service:"+"</span></html>"));
	    f.add(chSer);
	    
	    //gestion de l'espacement vers le bas
	    f.setLayout(new GridLayout(0, 1));
	    
	    
	    //declaration et ajout jComboBox
	   /* ArrayList<String> names=new ArrayList<String>();   
	    names.add("test1");
	    names.add("test2");
	    names.add("test3");
	    mycombo=new JComboBox(names.toArray());
        f.add(new JLabel ("<html><u><span style='font-size:20px'>"+"Service :"+"</span></html>"));
	    f.add(mycombo);
	    f.add(new JLabel(""));*/
	   
	    
	    //message de fin
	    //f.add(new JLabel ("<html><span style='font-size:15px'>"+"La Direction du Num�rique.\r\n" +"</span></html>"));

	    
	    //gestion esth�tique
	    try{
	        UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
	    }
	    catch(Exception e){
	            System.out.println("UIManager Exception : "+e);
	    }
	    Font font = new Font("Arial", Font.BOLD, 16);
	    Font font2 = new Font("Arial", Font.BOLD, 20);
	    f.getContentPane().add(p);
	    f.setSize(800,600);
	    f.setLocationRelativeTo(null);
	    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    f.setVisible(true);
	    f.setFont(font);
	    //mycombo.setFont(font);
		chNom.setFont(font);
		chBur.setFont(font);
		btn.setFont(font2);
		chSer.setFont(font);
		chTel.setFont(font);
		//f.setIconImages((List<? extends Image>) new ImageIcon("C:\\Users\\mlaunay\\eclipse-workspace\\recuptruc\\src\\recuptruc\\new.jpg").getImage());
		Image icon = Toolkit.getDefaultToolkit().getImage("C:\\Users\\mlaunay\\eclipse-workspace\\recuptruc\\src\\recuptruc\\new.jpg");    
		f.setIconImage(icon);
	    f.setTitle("Programme de recensement des postes"+"                                                                                                                                       "+"Direction Num�rique");
	    	
		//activation bouton ok 
		p.add(btn,new GridBagConstraints());
	    btn.addActionListener(new ActionListener() {
	    int dialogButton;
	    	
	    	//gestion de l'envoi des exceptions et cr�ation du fichier(sortie)
	    	public void actionPerformed(ActionEvent ae){
	    		if (chNom.getText().trim().isEmpty() || chBur.getText().trim().isEmpty()|| chSer.getText().trim().isEmpty()|| chTel.getText().trim().isEmpty()) {
	    			JOptionPane.showMessageDialog(null, "Veuillez remplir tous les champs!");
	    		}
	    		else {
	    			dialogButton = JOptionPane.showConfirmDialog (null, "Etes-vous s�re des informations inscrites ?","WARNING",JOptionPane.YES_NO_OPTION);
	                if(dialogButton == JOptionPane.YES_OPTION) {
	                	String service = chSer.getText();
		                String form = "[Nom/Prenom: "+chNom.getText() +"]\n "+"[Bureau: " +chBur.getText()+"] "+"[Service: " +service+"] "+"[Telephone: " +chTel.getText()+"] ";
		                //String service = "[Service: "+mycombo.getSelectedItem().toString()+"]";
		               // String servicechm = mycombo.getSelectedItem().toString();
		               // System.out.println(form);
		      		  	try {
			      		  	InetAddress ads = InetAddress.getLocalHost();
			                String nPc = ads.getHostName();
		      		  		String ph = pingmachine();
		      		  		nameFile = nPc+" - "+service;
		      		  		nameFileS = nPc;
                            //File fb = new File("\\storage2016.windows.dauphine.fr\\group\\Pedagogie\\CRIO\\Partage\\RECENS\\"+servicechm); 
                            //fb.mkdirs(); 		      		  		
                            //sortie2.createNewFile(); 		\\storage2016.windows.dauphine.fr\group\Pedagogie\CRIO\Partage\RECENS\\		C:\Users\mlaunay\Desktop\
							//sortie = new PrintWriter("\\storage2016.windows.dauphine.fr\\group\\Pedagogie\\CRIO\\Partage\\RECENS\\"+servicechm+"\\"+nPc+".txt","UTF-8");
		      		  		sortie = new PrintWriter("C:\\Users\\mlaunay\\Desktop\\\\"+nameFile+".txt","UTF-8");
							sortie.println(ph+form/*+service*/);
							//sortie.println (form);
							//sortie.println(service);
							sortie.close();
							JOptionPane.showMessageDialog(null, "Merci d'avoir pris le temps de r�pondre");
						} 
		      		  	catch (IOException e) {
							e.printStackTrace();
						}
		                System.exit(0);
	                	}
	    			}
	    		}
	    	});
	    f.add(new JLabel ("<html><u><p><span face=\"Arial\" style='color:#A4A4A4 ;font-size:12px;'>"+"En cas de probl�me pour remplir ce questionnaire veuillez contacter le 4444@dauphine.fr" +"</span></center></html>"));
	  	}
		 
		//foncion check fichier existant
		public static  String checkFileExist() throws IOException {
  		  	InetAddress ads = InetAddress.getLocalHost();
            String nPc = ads.getHostName();
  		  	return(nPc);

		}
		
	  //fonction qui emp�che de fermer/modifier la fenetre
	  public static void removeMinMaxClose(Component comp)
	  {
	    if(comp instanceof AbstractButton)
	    {
	      comp.getParent().remove(comp);
	    }
	    if (comp instanceof Container)
	    {
	      Component[] comps = ((Container)comp).getComponents();
	      for(int x = 0, y = comps.length; x < y; x++)
	      {
	        removeMinMaxClose(comps[x]);
	      }
	    }
	  }
	  
	  
	  //fonction ping des machine en silent + nomPc +nomUser
	  public static String pingmachine() throws IOException {
		  
		  InetAddress address = InetAddress.getLocalHost(); 
		  String hostIP = address.getHostAddress() ;
		  String hostName = address.getHostName();
		  String hostUser = System.getProperty("user.name" );
		  String ph = hostUser+" a r�pondu : "+"[IP: " + hostIP + "]\n " + "[Nom du Pc: " + hostName+"]\n ";
		  //System.out.println(ph);
		  return (ph);
	  }
	  
	  //fonction r�cup�ration et transfert vers fichier
	 /* public static void recuptransfert() throws IOException {
		  
		  PrintWriter writer = new PrintWriter("C:\\Users\\mathieu\\Desktop\\testIP.txt","UTF-8");
		  String ph = pingmachine();
		  //String fnb = buildGUI();
		  //writer.append(fnb);
		  sortie.append(ph);
		  sortie.close(); 		  
	  }*/
	  
	  //fonction  recherche fichier
	  public static void rechercheFichier() throws IOException {
		  File dir = new File("C:\\Users\\mlaunay\\Desktop\\");
	      FilenameFilter filter = new FilenameFilter() {
	         public boolean accept (File dir, String name) {
	        	 InetAddress ads = null;
				try {ads = InetAddress.getLocalHost();} catch (UnknownHostException e) {e.printStackTrace();}
	        	 String nPc = ads.getHostName();
	            return name.startsWith(nPc);
	         } 
	      }; 
	      String[] children = dir.list(filter); 
	      if (children == null) {
	         System.out.println("n'existe pas!"); 
	      } else { 
	         for (int i = 0; i< children.length; i++) {
	            String filename = children[i];
	            System.out.println(filename);
	            System.exit(0);
	         } 
	      } 
	   }
			  
	  //fonction main
	  public static void main(String[] args) throws IOException
	  {

	    SwingUtilities.invokeLater(new Runnable(){
	      public void run(){
	        new Questionnaire().buildGUI();
	      }
	    });
	    Questionnaire.rechercheFichier();
	    Questionnaire.pingmachine();
	  }
}